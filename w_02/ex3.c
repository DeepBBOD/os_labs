#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
	
	int n = atoi(argv[1]);
	
	for (int i = 0; i < n; i++){
		int offset = n-i;
		for (int j = 0; j < offset; j++){
			printf(" ");
		}
		for (int j = 0; j <= i*2; j++){
                        printf("*");
                }
		printf("\n");
	}
	
	return 0;
}

