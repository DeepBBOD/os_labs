#include <stdio.h>

void my_swap(int* a, int* b){
	int t = *a;
	*a = *b;
	*b = t;
}

int main(){
	int x, y;
	scanf("%i", &x);
	scanf("%i", &y);
	
	my_swap(&x, &y);

	printf("%i  %i\n", x, y);
	
return 0;
}
