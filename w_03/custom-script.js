$(document).ready(function () {
    $('#tickets_table').DataTable();
});
$('#tickets_table').DataTable({
    language: {
        processing: "Обработка",
        search: "Поиск",
        lengthMenu: "Отобразить _MENU_",
        info: "Заявки с _START_ по _END_ из _TOTAL_",
        infoEmpty: "Пусто",
        infoFiltered: "(_MAX_)",
        infoPostFix: "",
        loadingRecords: "Загрузка",
        zeroRecords: "Заявок с заданным параметром не найдено",
        emptyTable: "Пока что нет заявок",
        paginate: {
            first: "Первая",
            previous: "Предыдущая",
            next: "Следующая",
            last: "Последняя"
        },
        aria: {
            sortAscending: ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    },
    columnDefs: [
        {"className": "dt-center", "targets": "_all"}
    ],
    "lengthMenu": [25],
    "order": [[1, "desc"]]
});
$(document).ready(function () {
    $('ul.tabs').tabs();
    $('.modal').modal();
    $('input.autocomplete').autocomplete({
        data: {
            "Apple": null,
            "Apple1": null,
            "Apple3": null,
            "Apple4": null,
            "Apple5": null,
            "Apple6": null,
            "Microsoft": null,
            "Google": 'https://placehold.it/250x250'
        },
        limit: 5, // The max amount of results that can be shown at once. Default: Infinity.
        onAutocomplete: function (val) {
            // Callback function when value is autcompleted.
        },
        minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
    });
});