#include <stdio.h>
#include <stdlib.h>

void bubble_sort(int *arr, int size){
  int a, b, temp;

  for (a = 0; a < size; a++){
    for (b = a; b < size; b++){
      if (arr[a] > arr[b]){
        temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
      }
    }
  }
}

int main(){

  int *arr;
  int n, temp, i;

  scanf("%d", &n);
  arr = malloc(n*sizeof(int));

  for (i = 0; i < n; i++){
    scanf("%i", &temp);
    arr[i] = temp;
  }

  bubble_sort(arr, n);

  printf("%s\n", "\n\nResult: \n");

  for (int i = 0; i < n; i++){
    printf("%i\n", arr[i]);
  }

  return 0;
}
