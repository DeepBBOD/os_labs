#include <stdio.h>
#include <stdlib.h>

struct ListNode {
   int  value;
   struct ListNode *next;
};

void print_list(struct ListNode *node){
  struct ListNode *temp = node;

  while (temp != NULL){
    printf("%i\n", temp -> value);
    temp = temp -> next;
  }
}

void delete_node(struct ListNode *node){
  struct ListNode *temp = node;

  while (temp -> next -> next != NULL){
    temp = temp -> next;
  }
  temp -> next = NULL;
}


void insert_node(struct ListNode *node, int val){
  struct ListNode *temp = node;



  while (temp->next != NULL) {
    temp = temp -> next;
  }

  temp->next = malloc(sizeof(struct ListNode));
  temp->next->value = val;
  temp->next->next = NULL;
}

int main(int argc, char const *argv[]) {

  struct ListNode * head = (struct ListNode*) malloc(sizeof(struct ListNode));
  head -> value = 1;
  head -> next = NULL;

  insert_node(head, 3);
  insert_node(head, 5);
  delete_node(head);
  insert_node(head, 8);

  print_list(head);



  return 0;
}
