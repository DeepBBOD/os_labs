#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int execute_command(char **args)
{
  int i;
  if (args[0] == NULL) {
    return 1;
  }

  pid_t pid, wpid;
  int status;

  pid = fork();
  if (pid == 0) {
    // Child process
    if (execvp(args[0], args) == -1) {
      perror("Error");
    }
  } else if (pid < 0) {
    // Error forking
    perror("Error");
  } else {
    // Parent process
    do {
      wpid = waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}

int main(int argc, char **argv)
{

  char *line;
  char **args;
  int status;

  do {
    size_t bufsize = 0;
    getline(&line, &bufsize, stdin);

    int bufsize_split = 64, position = 0;
    char **tokens = malloc(bufsize_split * sizeof(char*));
    char *token;

    //It returns a pointer to the first token,it is return pointers to within the string we give it, and place \0 bytes at the end of each token.
    token = strtok(line, " \t\r\n\a");
    while (token != NULL) {
      tokens[position] = token;
      position++;


      token = strtok(NULL, " \t\r\n\a");
    }
    tokens[position] = NULL;

    args = tokens;

    status = execute_command(args);

    free(line);
    free(args);
  } while (status);
  return 0;
}
