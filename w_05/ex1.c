#include "stdio.h"
#include "pthread.h"
#include <unistd.h>

#define N 3

void *entry_point(void *arg){
  int id = *(int *)arg;
  printf("  Hello from thread %d\n", id);
  printf("  hi\n");
  sleep(1);
  pthread_exit(NULL);
}

void parallel() {
  pthread_t thread[N];
  int i;

  for (i = 0; i < N; i++) {
    printf("Run thread %d:\n", i);
    pthread_create(&thread[i], NULL, entry_point, &i);
  }

  for (i = 0; i < N; i++){
    pthread_join(thread[i], NULL);
  }
}

void sequense(){
  pthread_t thread;
  int i;

  for (i = 0; i < N; i++) {
    printf("Run thread %d:\n", i);
    pthread_create(&thread, NULL, entry_point, &i);
    pthread_join(thread, NULL);
  }
}

int main(int argc, char const *argv[]) {
  printf("%s\n", "Sequence mode:\n");
  sequense();


  printf("%s\n", "\n\nParallel mode:\n");
  parallel();

  return 0;
}
