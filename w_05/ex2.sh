tempfile=file.txt
lockfile=file.lock
i="0"
while [ $i -lt 4 ]
do
  if ln $tempfile $lockfile ; then
      OUTPUT="$(grep "." $tempfile | tail -1)"
      OUTPUT=$((OUTPUT + 1))
      echo $OUTPUT >> "$tempfile"
      rm $lockfile # after you're done
      sleep 1
  else
    sleep 1
  fi
done
