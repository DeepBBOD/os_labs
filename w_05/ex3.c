#include <stdio.h>
#include <stdlib.h>
#include "pthread.h"
#include "unistd.h"

typedef int bool;
#define STACK_SIZE 5
#define true 1
#define false 0

bool full;
bool empty;
int array[3];
int toppos = 0;

void *producer(void *inp_x){
  int *stack_pos = inp_x;
  while (true) {
    if(*stack_pos > STACK_SIZE || *stack_pos < 0){
      printf("%s\n", "error");
      pthread_exit(NULL);
    }
    *stack_pos += 1;
    printf("Prodced | stack top position: %d\n", *stack_pos);
    if( *stack_pos > 0){
      empty = 0;
    }
    if(*stack_pos == STACK_SIZE){
      full = true;
      empty = false;
    }
    while (full) {}
    sleep(rand() % 3);
  }
  pthread_exit(NULL);
}

void *consumer(void *inp_x){
  int *stack_pos = inp_x;
  while (true) {
    if(*stack_pos > STACK_SIZE || *stack_pos < 0){
      printf("%s\n", "error");
      pthread_exit(NULL);
    }
    printf("Consumed | stack top position: %d\n", *stack_pos);
    *stack_pos -= 1;
    if (*stack_pos < STACK_SIZE) {
      full = false;
    }
    if(*stack_pos == 0){
      empty = true;
      full = false;
    }
    while (empty) {}
    sleep(rand() % 3);
  }
  pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
  pthread_t consumer_thr;
  pthread_t produser_thr;
  time_t t;
  srand((unsigned) time(&t));
  int stack_pos = 0;
  int full = false;
  int empty = true;

  pthread_create(&consumer_thr, NULL , consumer, &stack_pos);
  pthread_create(&produser_thr, NULL , producer, &stack_pos);
  pthread_join(consumer_thr, NULL);
  pthread_join(produser_thr, NULL);
  return 0;
}
