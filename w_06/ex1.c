#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define str_len 64

int main(){
  char str1[str_len] = "I'm first string!";
  char str2[str_len] = "";

  int fd[2];

  pipe(fd);

  // Put value to pipe
  write(fd[1], str1, sizeof(str1));

  // Get this value
  read(fd[0], str2, sizeof(str2));

  close(fd[1]);
  close(fd[0]);

  printf("Recieved string: %s", str2);


  return(0);
}
