#include <stdio.h>
#include <signal.h>
#include "unistd.h"

void get_signal(int SIGID){
  printf("Ctrl+C handled");
}

int main(int argc, char const *argv[]) {
  signal(2, get_signal);

  sleep(10);


  return 0;
}
