#include <stdio.h>
#include <signal.h>
#include "unistd.h"

void get_signal_STOP(int SIGID){
  printf("Stop handled");
}

void get_signal_USR1(int SIGID){
  printf("USR1 handled");
}

void get_signal_KILL(int SIGID){
  printf("KILL handled");
}

int main(int argc, char const *argv[]) {
  signal(SIGTSTP, get_signal_STOP);
  signal(SIGUSR1, get_signal_USR1);
  signal(SIGKILL, get_signal_KILL);


  sleep(100);


  return 0;
}
