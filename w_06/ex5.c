#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[]) {
  int pid = fork();

  if (pid == 0){
    while (1) {
      printf("Hello from child\n");
      sleep(2);
    }
  }
  else{
    sleep(10);
    int kill_temp = kill(pid, SIGUSR1);
  }

  sleep(100);

  return 0;
}
