#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#define str_len 64

int main(){

  int fd[2], childStatus;
  pipe(fd);

  int pid1 = fork();
  int pid2;
  if (pid1 != 0){
    pid2 = fork();
  }

  if (pid1 == 0){
    char str2[str_len];
    printf("Child1\n");
    // Get this value
    read(fd[0], str2, sizeof(str2));

    sleep(3);

    int pid_temp = atoi(str2);
    kill(pid_temp, SIGTERM);
    printf("CHILD1: proccess %i terminated\n", pid_temp);
  }else if (pid2 == 0){
    printf("Child2\n");
    while(1){
      printf("CHILD2: still alive!\n");
      fflush(stdout);
      sleep(1);
    }
  }else{
    char str1[str_len];
    printf("Parent\n");
    sprintf(str1, "%i", pid2);
    // Put value to pipe
    write(fd[1], str1, sizeof(str1));
    waitpid(pid2, &childStatus, 0);
    printf("PARENT: ohh, smth happened to my second son :c \n");
    if (childStatus == 15){
      printf("PARENT: He was terminated! \n");
    }
    fflush(stdout);
  }
  exit(EXIT_SUCCESS);


  return(0);
}
