#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc/malloc.h>
#include <stdint.h>

void *my_realloc(void *pointer, size_t memory_to_be_allocated){
	void *result;

	if (pointer == NULL){
		result = malloc(memory_to_be_allocated);
	}
	else if (memory_to_be_allocated == 0){
		free(pointer);
	}
	else{
	  result = malloc(memory_to_be_allocated);
	  memcpy(result, pointer, malloc_size(pointer));
	  free(pointer);
	}

	/*
	I didn't get this line:
	Unless ptr is NULL, it must have been returned by an earlier call to malloc(), calloc() or realloc()
	*/

	return result;
}

int main(){

	int *arr = malloc(5*sizeof(int));

	for (int i=0; i<5; i++){
		arr[i] = 1337;
	}

	for (int i=0; i<5; i++){
		printf("%d\n", arr[i]);
	}

	printf("%s\n", "\n\nREALLOCATING\n============\n\n");

	arr = my_realloc(arr, 13*sizeof(int));

	for (int i=0; i<13; i++){
		printf("%d\n", arr[i]);
	}

	return 0;
}
