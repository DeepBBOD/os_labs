#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main(){
  struct rusage usage;
  int counter = 0;
  int *temp;
  while (counter < 10){
    temp = malloc(1024*1024*10);
    memset(temp, 0, 1024*1024*10);
    getrusage(RUSAGE_SELF, &usage);
    printf("%d\n", usage.ru_maxrss);
    sleep(1);
    counter++;
  }
  return(0);
}
