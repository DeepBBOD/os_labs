#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

struct _item {
    char page_num[100];
    int *age_array;
} item;

typedef struct _item *Item;

int find_least_used(Item *ptr, int num_of_pages, int free_pages) {
    int least_used = 0;

    for (int i = 1; i < num_of_pages - free_pages; i++) {
        for (int j = 0; j < num_of_pages; j++) {
            if (ptr[i]->age_array[j] < ptr[least_used]->age_array[j]) {
                least_used = i;
                j = num_of_pages;
            } else if (ptr[i]->age_array[j] > ptr[least_used]->age_array[j]) {
                j = num_of_pages;
            }
        }
    }

    return least_used;
}

int main() {
    char ch[100], file_name[25] = "Lab 09 input.txt";
    FILE *fp;
    int num_of_pages = 0;
    int free_pages = 0;
    int hit = 0, miss = 0;

    printf("%s\n", "Input number of pages: ");
    scanf("%d", &num_of_pages);
    free_pages = num_of_pages;

    Item *items_array = malloc(num_of_pages * sizeof(Item));

    fp = fopen(file_name, "r");
    if (fp == NULL) {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    while (fscanf(fp, " %100s", ch) == 1) {
        int flag = -1;
        for (int i = 0; i < num_of_pages - free_pages; i++) {
            if (!strcmp(items_array[i]->page_num, ch)) {
                flag = i;
                break;
            }
        }

        for (int i = 0; i < num_of_pages - free_pages; i++) {
            for (int j = num_of_pages - 1; j > 0; j--) {
                items_array[i]->age_array[j] = items_array[i]->age_array[j - 1];
            }
            if (i == flag) {
                hit++;
                items_array[i]->age_array[0] = 1;
            } else {
                items_array[i]->age_array[0] = 0;
            }
        }

        if (flag == -1) {
            miss++;
            if (free_pages > 0) {
                items_array[num_of_pages - free_pages] = NULL;
                items_array[num_of_pages - free_pages] = malloc(sizeof(struct _item));
                strcpy(items_array[num_of_pages - free_pages]->page_num, ch);
                items_array[num_of_pages - free_pages]->age_array = calloc(num_of_pages, sizeof(int));
                items_array[num_of_pages - free_pages]->age_array[0] = 1;
                free_pages--;
            } else {
                int least_used_index = find_least_used(items_array, num_of_pages, free_pages);
                items_array[least_used_index] = NULL;
                items_array[least_used_index] = malloc(sizeof(struct _item));
                strcpy(items_array[least_used_index]->page_num, ch);
                items_array[least_used_index]->age_array = calloc(num_of_pages, sizeof(int));
                items_array[least_used_index]->age_array[0] = 1;
            }
        }
    }

    printf("\n\n==========%s========\n\n", "");
    for (int i = 0; i < num_of_pages - free_pages; i++) {
        printf("%s: ", items_array[i]->page_num);
        for (int j = 0; j < num_of_pages; j++) {
            printf("%d", items_array[i]->age_array[j]);
        }
        printf("%s\n", "");
    }

    printf("\n\nhit: %d\nmiss:%d\n", hit, miss);

    return (0);
}
