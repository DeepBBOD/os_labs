#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

int main(){
    char my_str[] = "This is a noise gay";

    int res;
    int f_status;
    char *map;
    int fd;

    struct stat sb;

    fd = open("ex1.txt", O_RDWR);

    f_status = fstat(fd, &sb);

    res = lseek(fd, strlen(my_str) - 1, SEEK_SET);
    res = write(fd, "", 1);

    map = mmap(0, sb.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd , 0);

    int len = strlen(my_str);
    for (int i = 0; i < len; i++) {
        map[i] = my_str[i];
    }

    close(fd);

    return 0;
}
