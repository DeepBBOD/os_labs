#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>


int main() {
      int src_file, dest_file;
      char *src, *dest;
      struct stat buff;

      src_file = open("ex1.txt", 'rb');
      dest_file = open("ex1.memcpy.txt", 'wb');

      fstat (src_file, &buff);
      ftruncate(dest_file, buff.st_size);

      src = mmap(NULL, buff.st_size, PROT_READ, MAP_PRIVATE, src_file, 0);
      dest = mmap(NULL, buff.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, dest_file, 0);

      memcpy(dest, src, buff.st_size);

      close(src_file);
      close(dest_file);

      return 0;
}
